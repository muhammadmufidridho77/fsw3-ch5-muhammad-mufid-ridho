const express = require('express');
const path = require("path");

const app = express();

//mkae ejs
app.set('view engine', 'ejs');

app.use("/css", express.static(path.join(__dirname, "node_modules/mdb-ui-kit/css")));
app.use("/js", express.static(path.join(__dirname, "node_modules/mdb-ui-kit/js")));
app.use("/img", express.static(path.join(__dirname, "views/img")));
app.use("/css", express.static(path.join(__dirname, "views/css")));
app.use("/js", express.static(path.join(__dirname, "views/js")));
app.use("/src", express.static(path.join(__dirname, "views/src")));

app.get("/", (req,res) => {
    // res.sendFile(path.join(__dirname, "views/index.html"))
    res.render("index");
});

app.get("/About", (req,res) => {
    res.sendFile(path.join(__dirname, "views/about.html"))
});

app.listen(3000, () => {
    console.log("Server is running on port 3000");
});